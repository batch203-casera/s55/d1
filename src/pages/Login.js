import Button from 'react-bootstrap/Button';
import Form from 'react-bootstrap/Form';
import { useState, useEffect, useContext } from 'react';
import { Navigate } from 'react-router-dom';
import userContext from '../userContext';
import Swal from "sweetalert2";

function Login() {

    const { user, setUser } = useContext(userContext);

    const [isActive, setIsActive] = useState(false);
    const [formData, setFormData] = useState({
        email: "",
        password: ""
    });

    function handleChange(event) {
        const { name, value } = event.target;
        setFormData(prevFormData => {
            return {
                ...prevFormData,
                [name]: value
            }
        });
    }

    useEffect(() => {
        console.log(formData);
        if (
            formData.email.length > 0 &&
            formData.password.length > 0
        ) {
            setIsActive(true);
        } else {
            setIsActive(false);
        }
    }, [formData]);

    function authenticate(event) {

    }

    const retrieveUserDetails = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .then(response => response.json())
            .then(data => {
                setUser({
                    id: data._id,
                    isAdmin: data.isAdmin
                })
            })
    }

    function loginUser(event) {
        event.preventDefault();

        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({
                email: formData.email,
                password: formData.password
            })
        })
            .then(response => response.json())
            .then(data => {
                if (data.accessToken !== undefined) {
                    console.log(data.accessToken);
                    localStorage.setItem("token", data.accessToken);
                    retrieveUserDetails(data.accessToken);
                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "Welcome to Zuitt!"
                    })
                }else{
                    Swal.fire({
                        title: "Authentication failed!",
                        icon: "error",
                        text: "Check your login details and try again."
                    })
                }
            })
            .catch(err => console.log(err));

        // localStorage.setItem("email", formData.email);
        // setUser({
        //     email: localStorage.getItem("email")
        // })
        setFormData({
            email: "",
            password: "",
        });


    }

    return (
        (user.id !== null)
            ?
            <Navigate to="/courses" />
            :
            <>
                <h1 className='my-5 text-center'>Login</h1>
                <Form className="d-flex flex-column" onSubmit={loginUser}>
                    <Form.Group className="mb-3" controlId="email">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control type="email"
                            placeholder="Enter email"
                            name="email"
                            value={formData.email}
                            onChange={handleChange}
                            required />
                        <Form.Text className="text-muted">
                            We'll never share your email with anyone else.
                        </Form.Text>
                    </Form.Group>

                    <Form.Group className="mb-3" controlId="password">
                        <Form.Label>Password</Form.Label>
                        <Form.Control type="password" placeholder="Enter Password" name="password"
                            value={formData.password}
                            onChange={handleChange} required />
                    </Form.Group>

                    <Button variant={isActive ? "success" : "danger"} type="submit" id="submitBtn" className="col-2" disabled={!isActive}>
                        Login
                    </Button>

                </Form>
            </>
    );
}

export default Login;